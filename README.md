# Pros Projects
Progettazione di Sistemi Operativi, Danilo Bruschi @ Unimi

## Progetti AA 2020/2021
| Autore | Nome progetto | Link |
| ---    | ---           | ---  |
| Alberto Crosta, Matteo Zoia | ZCFS, implementation of log-structured filesystem for IoT devices | <https://gitlab.com/Mill_/zcfs> |
| Gianluca Nitti | Implementation of an USB-IDE/PATA converter on a STM32F4DISCOVERY | <https://github.com/gianluca-nitti/ide-usb> and <https://github.com/gianluca-nitti/ide-usb-pcb>  | 
| Andrea Monzani | VGA viewer | <https://github.com/mnznndr97/vga-viewer>  | 
| Federico Germinario, Giacomo Papaluca | Pleasent Ride | <https://github.com/federico-germinario/Pleasent_Ride>  | 
| Federica Paolì, Stefano Taverni | STM32 Speech recognition and traduction | <https://github.com/FedericaPaoli1/stm32-speech-recognition-and-traduction>  | 
